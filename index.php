<?php
/*
 * NOT FINISHED - not responsive when page width change
 * 
 * npush Website
 */
	$randomChannel = uniqid();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>n:push Newicon Ltd.</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>		
		<script type="text/javascript" src="http://fast.fonts.com/jsapi/e1c896c2-a59a-4de3-ac45-8ecb88eaa64a.js"></script>
		
        <style>
            .container {}
            
			.h40 {height:40px;}
			.h60 {height:60px;}
			.h80 {height:80px;}
			.h100 {height:100px;}
			.h110 {height:110px;}
			.h120 {height:120px;}
			.h160 {height:160px;}
			.h170 {height:170px;}
			
			.pl15 {padding-left:15px;}
			.pl80 {padding-left:80px;}
			.pl100 {padding-left:100px;}
			.pl140 {padding-left:140px;}
			.pl160 {padding-left:160px;}
			
			.p20 {padding:20px;}
			.p40 {padding:40px;}
			.m20 {margin:20px;}
			.m15 {margin:20px;}
			
			.ac {text-align:center;}
			
            body {background-color: #737577; color:#424241;font-family:HelveticaNeueW01-LtExt, Helvetica43-ExtendedLight, arial}

            .body-horizontal-top {height:1px;background-color:#636363;}
			.body-horizontal-bottom {height:1px;background-color:#ccc;}
            .logo {}
            .navbar .navbar-inner{border:0px;background-color:transparent;box-shadow:none;background-image:none;}
            .navbar .nav li a {text-transform:uppercase;padding:25px 25px 15px;font-family:Helvetica43-ExtendedLight, arial}
            
            .navbar .brand {padding:0px 5px 4px;}
			.npush-beatinfo {padding:21px 0px 10px 22px}
			
            h1,h2,h3,h4,h5,h6{font-family:helvetica neue;font-weight:500;}
            h1.heading {font-weight:200;color:#ffffff;text-transform:uppercase;font-size:35px;line-height:30px;margin-bottom:20px;letter-spacing:1px;}
			
			h2.heading-contact {font-family:HelveticaNeueW01-LtExt;font-size:22px;line-height:30px;letter-spacing:2px;font-weight:normal;color:#fff;}
			h2.heading-emph {font-family:georgia; font-size:25px;font-style:italic;color:#D7DF23;}
			h3.heading-emph {font-family:georgia; font-size:32px;font-style:italic;color:#D7DF23;}
			
			h4.heading {font-weight:500;color:#ffffff;font-size:18px;line-height:25px;margin-bottom:20px;letter-spacing:1px;}
			h4.footer {font-weight:100;color:#ffffff;font-size:14px;line-height:20px;margin:4px 0px 0px -11px;letter-spacing:1px;}
			h4.getstarted {margin:60px 0px 10px 0px;}
			
			h5.heading {font-weight:100;color:#ffffff;font-size:18px;line-height:25px;margin-bottom:20px;letter-spacing:1px;}
			h5.headinfo {margin-top:40px;}
			h5.heading-contact {font-family:HelveticaNeueW01-LtExt;font-size:15px;font-weight:normal;line-height:55px;color:#fff;}
            
            .hero-unit {padding:60px 0px;background-color:transparent;height:370px;}
            .callout{float:left;width:385px;}
            .callout p{font-size:16px;line-height:24px;letter-spacing:1px;}
			
			.widget{position:absolute;margin-left:520px;width:520px;height:408px;margin-top:-40px;background-image:url(images/get-message.png);}
			.widget-container{float:left;width:326px;height:170px;margin-top:70px;margin-left:160px;background-image:url(images/get-message-desktops.png);}
            
			.code-example {width:417px;height:263px;margin:auto;margin-top:20px;background-image:url(images/code-container.png);}
			.code-example-container {font-family:monaco;font-weight:500;font-weight:100;color:#ffffff;font-size:9px;line-height:35px;margin-bottom:20px;letter-spacing:1px;padding:22px 0px 0px 16px;position:relative;width:410px;height:100%;}
			
            .heading-emph{font-family:georgia; font-size:22px;font-style:italic;color:#D7DF23;}
            .emph {font-size:12px;color:#ffffff;line-height:24px;padding-top:10px;letter-spacing:1px;font-family:Helvetica43-ExtendedLight,arial;font-style:normal;font-weight:normal;}
			
            .highlight-boxes{padding:50px 30px 40px 30px;background-color:#636466;border-top:1px solid #ccc;border-bottom:1px solid #636363;}
            
            
            footer{background-color:#525251;border-top:4px solid #D7DF23;}
			
            /**            
			font-family:'Helvetica W01 Blk';
			font-family:'HelveticaNeueW01-LtExt';
			font-family:'HelveticaNeueW01-LtExtO';
			font-family:'HelveticaNeueETW01-45Lt';
			font-family:'HelveticaNeueETW01-55Rg';
            */
			
			.feature-image-box {border-right:0px solid #ccc;}
            .feature-image-box .caption{margin-bottom:35px;width:250px;}
			.feature-image-box img {display: block;max-width: 100%;margin-right: auto;margin-left: auto;}
			
			.bottom-button-home {margin-top:-2px;position:absolute;right:250px;}
			
			/* Large desktop */
			@media (min-width: 1200px) { 

				.widget{position:absolute;margin-left:520px;width:520px;height:408px;margin-top:-40px;}
				.bottom-button-home {right:250px;}
			}

			/* Large desktop */
			@media (min-width: 980px) and (max-width: 1200px) { 
				.navbar .nav li a {padding:25px 15px 15px;}
				.navbar .navbar-inner{padding-right:0px;}
				
				.widget{margn-top:-260px;margin-left:320px;position:absolute;width:520px;height:408px;margin-top:-40px;}
				.bottom-button-home {right:150px;}
				
				
			}

			/* Portrait tablet to landscape and desktop */
			/* iPAD */
			@media (min-width: 768px) and (max-width: 979px) { 
				.navbar .navbar-inner{padding:0px 0px 0px 0px;}
				
				.bottom-button-home {right:150px;}
				
				.widget{display:none;}
				.code-example {display:none;}
			}

			/* Landscape phone to portrait tablet */
			@media (max-width: 767px) { 
				/* for some reason bootstrap think it sensible to add padding to the body at this point. */
				body {padding:0px;}
				.navbar .navbar-inner{padding:0px 0px 0px 0px;}
				.container {padding:0px 20px}
				
				.widget{display:none;}
				.bottom-button-home {right:100px;}
			}

			/* Landscape phones and down */
			@media (max-width: 480px) { 
				/* for some reason bootstrap think it sensible to add padding to the body at this point. */
				body {padding:0px;}
				.navbar .navbar-inner{padding:0px 0px 0px 0px;}
				.container {padding:0px 20px}
				
				.widget{display:none;}
				.bottom-button-home {right:50px;}
			}

		</style>
        <link type="text/css" rel="stylesheet" href="http://fast.fonts.com/cssapi/e1c896c2-a59a-4de3-ac45-8ecb88eaa64a.css"/>
        <script type="text/javascript" src="http://fast.fonts.com/jsapi/e1c896c2-a59a-4de3-ac45-8ecb88eaa64a.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <header>
            <div style="height:4px;background-color:#D7DF23;"></div>
            <div class="container">
				<div class="navbar ">
					<div class="navbar-inner">
						<a class="brand newicon-logo" href="/" style="background-color:#D7DF23;">
							<img src="images/npush-logo.png" />
						</a>
						<a>
							<img  class="npush-beatinfo" src="images/head-info.png" />
						</a>						
					</div>
				</div>
            </div>
        </header>
        <div class="container">
            <!-- Main hero unit for a primary marketing message or call to action -->
            <div class="hero-unit">
                <div class="callout">
                    <h1 class="heading">PUSH NOTIFICATION,</h1>
					<h1 class="heading">DONE RIGHT</h1>
                    <h5 class="heading headinfo">
						Get your apps talking with n:push&trade;.
						The intuitive, robust way to send instant notifications from your apps.
					</h5>
					<h4 class="heading getstarted">GET STARTED</h4>
					<a class="brand" href="#ModuleNPushDownloadModal" data-toggle="modal">
						<img src="images/button-getapi.png" />
					</a>					
                </div>
				<div class="widget">
					<!--- banner as background --->
					<div class="widget-container">
						<!--- banner as background --->	
					</div>
				</div>
            </div>
        </div> <!-- /container -->
		<div class="body-horizontal-top"></div>            
        <div class="highlight-boxes">
            <!-- Example row of columns -->
            <div class="container">
                <div class="row-fluid">
                    <div class="span4">
						<div class="feature-image-box">
							<div class="caption">
								<p class="heading-emph">Versatile development</p>
								<p class="emph">Lorem Ipsum is simply dummy text of the printing and typesetting industry..</p>
							</div>
						</div>
                    </div>
                    <div class="span4">
						<div class="feature-image-box">
                            <div class="caption">
                                <p class="heading-emph">Easy integration</p>
                                <p class="emph">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                            </div>
						</div>
                   </div>
                    <div class="span4">
                        <div class="feature-image-box">
                            <div class="caption">
                                <p class="heading-emph">Fast development</p>
                                <p class="emph">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="body-horizontal-bottom"></div> 
        <div class="container">
			<div class="row-fluid"><div class="h100"></div></div>
			<div class="row-fluid">
				<div class="span6">
					<h2 class="heading-emph pl100">Subscribe</h2>
					<h5 class="pl100 hide">using JavaScript</h5>
				</div>			
				<div class="span6">
					<h2 class="heading-emph pl100">Publish</h2>
					<h5 class="pl100 hide">using JavaScript</h5>
				</div>							
			</div>			
			<div class="row-fluid">
				<div class="span6">
					<div>
						<div class="code-example">
							<div class="code-example-container">
									&lt;script src="http://app.newicon.net/np.js"&gt;&lt/script&gt;
									<br>npush.init(<span id="ModuleNPusherTempAPI"><storng>wait for you API - generating...</strong></span>);
									<br>var mychannel = npush.subscribe('mychannel');
									<br>mychannel.on('message', function(msg) { alert(msg); });
							</div>
						</div>
					</div>					
				</div>
				<div class="span6">
					<div>
						<div class="code-example">
							<div class="code-example-container" style="padding-left:15px;">
								&nbsp;&nbsp;mychannel.trigger('message', 'Hello world!');
							</div>
						</div>
					</div>					
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					<h4 class="pl100">Java Script</h4>
				</div>			
				<div class="span6">
					<h4 class="pl100">Java Script</h4>
				</div>							
			</div>
			<div class="row-fluid h80"></div>
			<div class="row-fluid">
				<div class="span12 ac">
					<a class="brand" href="#ModuleNPushTryModal" data-toggle="modal">
						<img src="images/button-try-it.png" />
					</a>
				</div>
			</div>
			
			<br />
			
			<div class="container" style="width:804px;" >
				<div class="pull-left" id="canvasDiv" style="cursor:crosshair;background-color: #fff;border:1px solid #000;width:400px;height:400px;"></div>
				<div class="pull-left" id="canvasDiv2" style="cursor:crosshair;background-color: #fff;border:1px solid #000;width:400px;height:400px;"></div>
			</div>
			
			<div class="row-fluid h120"></div>
			<div class="row-fluid">
				<div class="span12">
					<h3 class="heading-emph ac">Need help?</h3>
				</div>			
			</div>		
			<div class="row-fluid">
				<div class="span12">
					<h2 class="heading-contact ac">Contact: support@npush.net</h2>
					<h2 class="heading-contact ac">or call us on 0117 957 5306</h2>
					<h5 class="heading-contact ac">Between 9am and 5pm Mondays to Friday (except public holidays)</h5>
				</div>			
			</div>	
			<div class="row-fluid h120"></div>
        </div>
		
		<!-- Demo -->
		
        <footer style="background-color: #636466;">
            <div class="container">
				<div class="row-fluid">
					<div class="span12">
						<a href="/">
							<img class="bottom-button-home" src="images/button-go-top.png" />
						</a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span2" style="width:142px;">
						<a class="brand newicon-logo" href="/">
							<img src="images/newicon-logo.png" />
						</a>			
					</div>			
					<div class="span4" style="padding: 0px;">
						<h4 class="footer">nPush is  development by newicon Ltd.<br/>Find our more about us…</h4>
					</div>
					<div class="span6">
						
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12"></div>
				</div>		
				<div class="row-fluid">
					<div class="span12"></div>
				</div>						
            </div>
        </footer>      
		
		<!-- Modal windows -->
		<div class="modal fade in hide" id="ModuleNPushTryModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Copy and paste in your terminal</h3>
			</div>
			<div class="modal-body h170">
				<pre class="h100 m20">curl -H "Content-Type: application/post" -d '{"event":"message", "data":"Hello World"}' "http://push.vm.newicon.net:80/api/channel/<?php echo $randomChannel; ?>/api/50b37b453460511760000001/version/2.0/t/<?php echo time(); ?>/suid/1/suname/demo"</pre>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-dismiss="modal">OK</a>
			</div>
		</div>
		
		<div class="modal fade in hide" id="ModuleNPushDownloadModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>One step download</h3>
			</div>
			<div class="modal-body">
				<div class="row-fluid">
					<div class="span12 ac p20">
						<a href="#" class="btn btn-success btn-large" id="ModuleNPushActionDownloadLibrary">Download npush.js</a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12 p20">
						<h4>Useful tips:</h4>
						<ul>
							<li>
								Add this tag to your html file
								<pre style="width:90%;">&lt;script src="path/to/npush.js"&gt;&lt;script&gt;</pre>
							</li>
							<li>Use example from main page to start pushing your messages...</li>
							<li>To change options like API key, server settings, userID you have to edit npush.js file</li>
							<li>To production use we recommend to download compressed library <a href="#" id="ModuleNPushActionDownloadLibraryMin"><strong>npush.min.js</strong></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
			</div>
		</div>		
		
		<!-- Downloader -->
		<iframe id="ModuleHeaderIFrame" style="display:none"></iframe>
		
		<!-- JavaScript Resources -->
		<script src="js/vendor/jquery-1.8.2.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>		
		<script src="js/vendor/underscore-min.js"></script>		
		<script src="js/npush/npush.js"></script>		
		
		<!-- Init engine -->
		
		<script>
			
			var ModuleServerURL = 'http://npushadmin.app.newicon.net';
			//var ModuleServerURL = 'http://localhost/npushwebsite/public/htdocs';
			var ModuleNPushDemoChannelName = '<?php echo $randomChannel; ?>';
			var ModuleNPushDemoChannelAPI = '';

			/*
			* Generate new API key for client
			*/
			function ModuleNPushGenerateAPI(_type, _action) {
				if(ModuleNPushDemoChannelAPI == '') {
					$.getJSON(ModuleServerURL + '/pusher/manager/compileLibraryGetAPI' + '?callback=?', function(data) {	
						ModuleNPushDemoChannelAPI = data.$id;		
						$('#ModuleNPusherTempAPI').html("'" + ModuleNPushDemoChannelAPI + "'");
						if(_action == 1) {
							if(ModuleNPushDemoChannelAPI != undefined && ModuleNPushDemoChannelAPI != '') {
								ModuleNPushDownloadLibraryWithSettings(_api, _type);
							}
						}
					}
					).success(function() {
						// nothing to do
					}).error(function(err) {
						console.log(err);
						ModuleNPushDemoChannelAPI = '';
						$('#ModuleNPusherTempAPI').html("ERROR: cannot get your API. Contact us! Sorry...");
					});
				} else {
					if(_action == 1) {
						ModuleNPushDownloadLibraryWithSettings(ModuleNPushDemoChannelAPI, _type);
					}
				}
				return false;
			}

			/*
			* Demo channel subscribe
			*/
			function ModuleNPushInitDemo() {
				var channel = npush.subscribe(ModuleNPushDemoChannelName);
				channel.on('message', function(data) { alert(data); });
			}
	
			// RESOURCES
			function ModuleNPushDownloadLibraryWithSettings(_api, _type) {
				var ifrm = document.getElementById('ModuleHeaderIFrame');
				ifrm.src = ModuleServerURL + '/pusher/manager/compileLibrary/apiId/' + _api + '/type/' + _type;		
			}			

			/*
			* Init
			*/
			$(document).ready(function() {
				ModuleNPushInitDemo();
				ModuleNPushGenerateAPI(0,0);
				$('#ModuleNPushActionDownloadLibrary').click(function(){ModuleNPushGenerateAPI('full',1)});
				$('#ModuleNPushActionDownloadLibraryMin').click(function(){ModuleNPushGenerateAPI('min',1)});
			});
			
			/*
			 * Common
			 */
			function respondServer(_respond, _code) {
				var $response=$(_respond);
				return $response.find("#"+_code).html();		
			}
			
			// --- Canvas Demo ---- //
			
			var chanDraw = npush.subscribe('mychannel'); 

			var pushSentCount = 0;
			var pushReceiveCount = 0;

			// canvas 1 
			var canvasDiv = document.getElementById('canvasDiv');
			canvas = document.createElement('canvas');
			canvas.setAttribute('width', 400);
			canvas.setAttribute('height', 400);
			canvas.setAttribute('id', 'canvas');
			canvasDiv.appendChild(canvas);
			if(typeof G_vmlCanvasManager != 'undefined') {
				canvas = G_vmlCanvasManager.initElement(canvas);
			}
			context = canvas.getContext("2d");

			// canvas 2
			var canvasDiv2 = document.getElementById('canvasDiv2');
			canvas2 = document.createElement('canvas');
			canvas2.setAttribute('width', 400);
			canvas2.setAttribute('height', 400);
			canvas2.setAttribute('id', 'canvas2');
			canvasDiv2.appendChild(canvas2);
			if(typeof G_vmlCanvasManager != 'undefined') {
				canvas2 = G_vmlCanvasManager.initElement(canvas2);
			}
			context2 = canvas2.getContext("2d");


			$('#canvas').mousedown(function(e){
				var mouseX = e.pageX - this.offsetLeft;
				var mouseY = e.pageY - this.offsetTop;

				paint = true;
				addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, false, true);
				redraw();
			});

			$('#canvas').mousemove(function(e){
				if(paint){
					addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true, true);
					redraw();
				}
			});

			$('#canvas').mouseup(function(e){
				paint = false;
			});

			$('#canvas').mouseleave(function(e){
				paint = false;
			});

			var clickX = new Array();
			var clickY = new Array();
			var clickDrag = new Array();
			var paint;

			var colorPurple = "#cb3594";
			var colorGreen = "#D7DF23";
			var colorYellow = "#ffcf33";
			var colorBrown = "#986928";

			var curColor = colorGreen;
			var clickColor = new Array();
			var curColor2 = colorGreen;
			var clickColor2 = new Array();

			function addClick(x, y, dragging, sendPush)
			{
				clickX.push(x);
				clickY.push(y);
				clickDrag.push(dragging);
				clickColor.push(curColor);
				if (sendPush) {
					pushSentCount ++;
					console.log('push sent ', pushSentCount);
					chanDraw.trigger('draw2', {x:x,y:y,dragging:dragging,curColor:curColor});
				}
			}

			function addClick2(x, y, dragging, sendPush)
			{
				clickX2.push(x);
				clickY2.push(y);
				clickDrag2.push(dragging);
				clickColor2.push(curColor);
				if (sendPush) {
					pushSentCount ++;
					console.log('push sent ', pushSentCount);
					chanDraw.trigger('draw1', {x:x,y:y,dragging:dragging,curColor:curColor});
				}
			}

			chanDraw.on('draw2', function(data) {
				pushReceiveCount ++;
				console.log('push received count', pushReceiveCount);
				clickColor.push(data.curColor);
				addClick2(data.x, data.y, data.dragging, false);
				redraw2();
			});
			chanDraw.on('draw1', function(data) {
				pushReceiveCount ++;
				console.log('push received count', pushReceiveCount);
				addClick(data.x, data.y, data.dragging, false);
				redraw();
			});

			function redraw(){

				canvas.width = canvas.width; // Clears the canvas

				context.strokeStyle = "#df4b26";
				context.lineJoin = "round";
				context.lineWidth = 5;

				for(var i=0; i < clickX.length; i++)
				{		
					context.beginPath();
					if(clickDrag[i] && i){
						context.moveTo(clickX[i-1], clickY[i-1]);
					}else{
						context.moveTo(clickX[i]-1, clickY[i]);
					}
					context.lineTo(clickX[i], clickY[i]);
					context.closePath();
					context.strokeStyle = clickColor[i];
					context.stroke();
				}
			}

			function redraw2(){
				canvas2.width = canvas2.width; // Clears the canvas

				context2.strokeStyle = "#df4b26";
				context2.lineJoin = "round";
				context2.lineWidth = 5;

				for(var i=0; i < clickX2.length; i++)
				{		
					context2.beginPath();
					if(clickDrag2[i] && i){
						context2.moveTo(clickX2[i-1], clickY2[i-1]);
					}else{
						context2.moveTo(clickX2[i]-1, clickY2[i]);
					}
					context2.lineTo(clickX2[i], clickY2[i]);
					context2.closePath();
					context2.strokeStyle = clickColor2[i];
					context2.stroke();
				}
			}

			$('#canvas2').mousedown(function(e){
				var mouseX = e.pageX - this.offsetLeft;
				var mouseY = e.pageY - this.offsetTop;

				paint2 = true;
				addClick2(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, false, true);
				redraw2();
			});

			$('#canvas2').mousemove(function(e){
				if(paint2){
					addClick2(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true, true);
					redraw2();
				}
			});

			$('#canvas2').mouseup(function(e){
				paint2 = false;
			});

			$('#canvas2').mouseleave(function(e){
				paint2 = false;
			});

			var clickX2 = new Array();
			var clickY2 = new Array();
			var clickDrag2 = new Array();
			var paint2;

			$( "#canvasDiv, #canvasDiv2" ).mousedown(function(event){
				event.preventDefault();
			});
		</script>		
		<?php require_once('js/main.js'); ?>
    </body>
</html>
