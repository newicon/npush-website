N:Push
======

N:Push website currently on npush.app.newicon.net store the html website that promotes the npush push notifications service.

The N:Push website in the future may become the multi-tennant system enabling people to sign in, generate API keys and manage billing for useage. 

Related repos:
 
 - npush-website
 - npush-admin (Admin panel to monitor npush service)
 - npush-server (Contains the magic)
 - npush-module (server-side NodeJS module)
 - npush-client (client-side JS library)
