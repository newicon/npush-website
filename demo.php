<?php

/*
 * NOT FINISHED - not responsive when page width change
 * 
 * npush Website
 */

	$randomChannel = uniqid();

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>n:push Newicon Ltd.</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>		
		<script src="http://fast.fonts.com/jsapi/e1c896c2-a59a-4de3-ac45-8ecb88eaa64a.js" type="text/javascript"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js" type="text/javascript"></script>
        <style>
            .container {}
            
			.h40 {height:40px;}
			.h60 {height:60px;}
			.h80 {height:80px;}
			.h100 {height:100px;}
			.h110 {height:110px;}
			.h120 {height:120px;}
			.h160 {height:160px;}
			.h170 {height:170px;}
			
			.pl15 {padding-left:15px;}
			.pl80 {padding-left:80px;}
			.pl100 {padding-left:100px;}
			.pl140 {padding-left:140px;}
			.pl160 {padding-left:160px;}
			
			.p20 {padding:20px;}
			.p40 {padding:40px;}
			.m20 {margin:20px;}
			.m15 {margin:20px;}
			
			.ac {text-align:center;}
			
            body {background-color: #737577; color:#424241;font-family:HelveticaNeueW01-LtExt, Helvetica43-ExtendedLight, arial}

            .body-horizontal-top {height:1px;background-color:#636363;}
			.body-horizontal-bottom {height:1px;background-color:#ccc;}
            .logo {}
            .navbar .navbar-inner{border:0px;background-color:transparent;box-shadow:none;background-image:none;}
            .navbar .nav li a {text-transform:uppercase;padding:25px 25px 15px;font-family:Helvetica43-ExtendedLight, arial}
            
            .navbar .brand {padding:0px 5px 4px;}
			.npush-beatinfo {padding:21px 0px 10px 22px}
			
            h1,h2,h3,h4,h5,h6{font-family:helvetica neue;font-weight:500;}
            h1.heading {font-weight:200;color:#ffffff;text-transform:uppercase;font-size:35px;line-height:30px;margin-bottom:20px;letter-spacing:1px;}
			
			h2.heading-contact {font-family:HelveticaNeueW01-LtExt;font-size:22px;line-height:30px;letter-spacing:2px;font-weight:normal;color:#fff;}
			h2.heading-emph {font-family:georgia; font-size:25px;font-style:italic;color:#D7DF23;}
			h3.heading-emph {font-family:georgia; font-size:32px;font-style:italic;color:#D7DF23;}
			
			h4.heading {font-weight:500;color:#ffffff;font-size:18px;line-height:25px;margin-bottom:20px;letter-spacing:1px;}
			h4.footer {font-weight:100;color:#ffffff;font-size:14px;line-height:20px;margin:4px 0px 0px -11px;letter-spacing:1px;}
			h4.getstarted {margin:60px 0px 10px 0px;}
			
			h5.heading {font-weight:100;color:#ffffff;font-size:18px;line-height:25px;margin-bottom:20px;letter-spacing:1px;}
			h5.headinfo {margin-top:40px;}
			h5.heading-contact {font-family:HelveticaNeueW01-LtExt;font-size:15px;font-weight:normal;line-height:55px;color:#fff;}
            
            .hero-unit {padding:60px 0px;background-color:transparent;height:370px;}
            .callout{float:left;width:385px;}
            .callout p{font-size:16px;line-height:24px;letter-spacing:1px;}
			
			.widget{position:absolute;margin-left:520px;width:520px;height:408px;margin-top:-40px;background-image:url(images/get-message.png);}
			.widget-container{float:left;width:326px;height:170px;margin-top:70px;margin-left:160px;background-image:url(images/get-message-desktops.png);}
            
			.code-example {width:417px;height:263px;margin:auto;margin-top:20px;background-image:url(images/code-container.png);}
			.code-example-container {font-family:monaco;font-weight:500;font-weight:100;color:#ffffff;font-size:9px;line-height:35px;margin-bottom:20px;letter-spacing:1px;padding:22px 0px 0px 16px;position:relative;width:410px;height:100%;}
			
            .heading-emph{font-family:georgia; font-size:22px;font-style:italic;color:#D7DF23;}
            .emph {font-size:12px;color:#ffffff;line-height:24px;padding-top:10px;letter-spacing:1px;font-family:Helvetica43-ExtendedLight,arial;font-style:normal;font-weight:normal;}
			
            .highlight-boxes{padding:50px 30px 40px 30px;background-color:#636466;border-top:1px solid #ccc;border-bottom:1px solid #636363;}
            
            
            footer{background-color:#525251;border-top:4px solid #D7DF23;}
			
            /**            
			font-family:'Helvetica W01 Blk';
			font-family:'HelveticaNeueW01-LtExt';
			font-family:'HelveticaNeueW01-LtExtO';
			font-family:'HelveticaNeueETW01-45Lt';
			font-family:'HelveticaNeueETW01-55Rg';
            */
			
			.feature-image-box {border-right:0px solid #ccc;}
            .feature-image-box .caption{margin-bottom:35px;width:250px;}
			.feature-image-box img {display: block;max-width: 100%;margin-right: auto;margin-left: auto;}
			
			.bottom-button-home {margin-top:-2px;position:absolute;right:250px;}
			
			/* Large desktop */
			@media (min-width: 1200px) { 

				.widget{position:absolute;margin-left:520px;width:520px;height:408px;margin-top:-40px;}
				.bottom-button-home {right:250px;}
			}

			/* Large desktop */
			@media (min-width: 980px) and (max-width: 1200px) { 
				.navbar .nav li a {padding:25px 15px 15px;}
				.navbar .navbar-inner{padding-right:0px;}
				
				.widget{margn-top:-260px;margin-left:320px;position:absolute;width:520px;height:408px;margin-top:-40px;}
				.bottom-button-home {right:150px;}
				
				
			}

			/* Portrait tablet to landscape and desktop */
			/* iPAD */
			@media (min-width: 768px) and (max-width: 979px) { 
				.navbar .navbar-inner{padding:0px 0px 0px 0px;}
				
				.bottom-button-home {right:150px;}
				
				.widget{display:none;}
				.code-example {display:none;}
			}

			/* Landscape phone to portrait tablet */
			@media (max-width: 767px) { 
				/* for some reason bootstrap think it sensible to add padding to the body at this point. */
				body {padding:0px;}
				.navbar .navbar-inner{padding:0px 0px 0px 0px;}
				.container {padding:0px 20px}
				
				.widget{display:none;}
				.bottom-button-home {right:100px;}
			}

			/* Landscape phones and down */
			@media (max-width: 480px) { 
				/* for some reason bootstrap think it sensible to add padding to the body at this point. */
				body {padding:0px;}
				.navbar .navbar-inner{padding:0px 0px 0px 0px;}
				.container {padding:0px 20px}
				
				.widget{display:none;}
				.bottom-button-home {right:50px;}
			}
			
			#canvasDiv{cursor:crosshair;}

		</style>
        <link type="text/css" rel="stylesheet" href="http://fast.fonts.com/cssapi/e1c896c2-a59a-4de3-ac45-8ecb88eaa64a.css"/>
        <script type="text/javascript" src="http://fast.fonts.com/jsapi/e1c896c2-a59a-4de3-ac45-8ecb88eaa64a.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <header>
            <div style="height:4px;background-color:#D7DF23;"></div>
            <div class="container">
				<div class="navbar ">
					<div class="navbar-inner">
						<a class="brand newicon-logo" href="/" style="background-color:#D7DF23;">
							<img src="images/npush-logo.png" />
						</a>
						<a>
							<img class="npush-beatinfo" src="images/head-info.png" />
						</a>						
					</div>
				</div>
            </div>
        </header>
        <div class="container">
            <!-- Main hero unit for a primary marketing message or call to action -->
            <div class="hero-unit">
				<div class="btn-group">
					<button class="btn purple color" data-color="#cb3594">purple</button><button class="btn green color" data-color="#D7DF23">Green</button><button class="btn yellow color" data-color="#ffcf33">Yellow</button><button class="btn brown color" data-color="#986928">Brown</button>
				</div>
				<button class="btn clear">Clear</button>
				<div id="canvasDiv" style="background-color: #fff;border:1px solid #000;width:700px;height:500px;"></div>
            </div>
        </div> <!-- /container -->
		
		
		
		
		<!-- JavaScript Resources -->
		<script src="js/vendor/jquery-1.8.2.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>		
		<script src="js/vendor/underscore-min.js"></script>		
		<script src="js/npush/npush.js"></script>		
		
		<!-- Init engine -->
		
		<script>

			// --- Canvas Demo ---- //
			npush.init({apiKey:'515eb5d83460514e7e000076'}); 

			var chanDraw = npush.subscribe('mychannel'); 
			chanDraw.activity(100000);

			var pushSentCount = 0;
			var pushReceiveCount = 0;

			// canvas 1 
			var canvasDiv = document.getElementById('canvasDiv');
			canvas = document.createElement('canvas');
			canvas.setAttribute('width', 700);
			canvas.setAttribute('height', 500);
			canvas.setAttribute('id', 'canvas');
			canvasDiv.appendChild(canvas);
			if(typeof G_vmlCanvasManager != 'undefined') {
				canvas = G_vmlCanvasManager.initElement(canvas);
			}
			context = canvas.getContext("2d");

				
			press = function (e) {
				var mouseX = e.pageX - this.offsetLeft;
				var mouseY = e.pageY - this.offsetTop;

				paint = true;
				addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, false, true);
				redraw();
			}

			drag = function (e) {
				if(paint){
					addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true, true);
					redraw();
				}
				// Prevent the whole page from dragging if on mobile
				e.preventDefault();
			},

			release = function () {
				paint = false;
			},

			cancel = function () {
				paint = false;
			};

			// Add mouse event listeners to canvas element
			canvas.addEventListener("mousedown", press, false);
			canvas.addEventListener("mousemove", drag, false);
			canvas.addEventListener("mouseup", release);
			canvas.addEventListener("mouseout", cancel, false);

			// Add touch event listeners to canvas element
			canvas.addEventListener("touchstart", press, false);
			canvas.addEventListener("touchmove", drag, false);
			canvas.addEventListener("touchend", release, false);
			canvas.addEventListener("touchcancel", cancel, false);
			
			var clickX = {};
			var clickY = {};
			var clickDrag = {};
			var users = {};
			
			var paint;

			var colorPurple = "#cb3594";
			var colorGreen = "#D7DF23";
			var colorYellow = "#ffcf33";
			var colorBrown = "#986928";

			var curColor = colorGreen;
			var clickColor = {};
			
			$('.btn.color').click(function(e){
				curColor = $(this).attr('data-color');
			});
			$('.btn.clear').click(function(e){
				canvas.width = canvas.width;
				clickX = {};
				clickY = {};
				clickDrag = {};
				clickColor = {}
			});

			function addClick(x, y, dragging, sendPush)
			{
				if (sendPush) {
					pushSentCount ++;
					console.log('push sent ', pushSentCount);
					chanDraw.trigger('draw', {x:x,y:y,dragging:dragging,curColor:curColor});
				}
			}

			chanDraw.on('draw', function(data, sys) {
				pushReceiveCount ++;
				users[sys.user_id] = sys.user_id;
				if (clickX[sys.user_id] == undefined) {
					clickX[sys.user_id] = [];
				}
				if (clickY[sys.user_id] == undefined) {
					clickY[sys.user_id] = [];
				}
				if (clickDrag[sys.user_id] == undefined) {
					clickDrag[sys.user_id] = [];
				}
				if (clickColor[sys.user_id] ==  undefined) {
					clickColor[sys.user_id] = [];
				}
				
				clickX[sys.user_id].push(data.x);
				clickY[sys.user_id].push(data.y);
				clickDrag[sys.user_id].push(data.dragging);
				clickColor[sys.user_id].push(data.curColor);
				console.log('push received count', pushReceiveCount);
				redraw();
			});

			function redraw(){

				canvas.width = canvas.width; // Clears the canvas

				context.strokeStyle = "#df4b26";
				context.lineJoin = "round";
				context.lineWidth = 5;
				console.log(users)
				console.log(clickX)
				_.forEach(users, function(user){
					var userId = user;
					console.log(user)
					for(var i=0; i < clickX[userId].length; i++)
					{		
						context.beginPath();
						if(clickDrag[userId][i] && i){
							context.moveTo(clickX[userId][i-1], clickY[userId][i-1]);
						}else{
							context.moveTo(clickX[userId][i]-1, clickY[userId][i]);
						}
						context.lineTo(clickX[userId][i], clickY[userId][i]);
						context.closePath();
						context.strokeStyle = clickColor[userId][i];
						context.stroke();
					}
				});
			}
			
			$( "#canvasDiv" ).mousedown(function(event){
				event.preventDefault();
			});


		</script>		
		
		<?php require_once('js/main.js'); ?>
		
    </body>
</html>
